import netCDF4
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from datetime import date, datetime, timedelta
import os
import csv
import shutil
from tqdm import tqdm

EARTH_DATA_LINK = "https://urs.earthdata.nasa.gov/oauth/authorize?response_" \
                 "type=code&redirect_uri=https%3A%2F%2Fdisc.gsfc.nasa.gov%2Flogin%2Fcallback&client_" \
                 "id=C_kKX7TXHiCUqzt352ZwTQ"
DOWNLOAD = '/home/amman/Downloads' #insert you directory path please.
DATA_FILE = "M2T1NXSLV"
USERNAME = "Amman"
PASSWORD = "Hello2u!"
FROM_DATE = "2019-01-01"
TO_DATE = "2019-01-02"
fields = ['U50M', 'V50M']
yr_corr = {
        2019: 3,
        2018: 2,
        2017: 1,
        2016: 0,
}


def write_field(v, field, fd):
    print("writing particular field")
    v_temp = v[:]
    hr = 1
    row_index = 1
    col_index = 1
    with open(field+'.csv', 'a') as f:
        f = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        f.writerow([field, fd.year, fd.month, fd.day])
        for hour in v_temp:
            for row in hour:
                for cell in row:
                    r = [hr, row_index, col_index, cell]
                    f.writerow(r)
                    col_index += 1
                col_index = 1
                row_index += 1
            row_index = 1
            hr += 1


def writing_files_to_csv(file_dates):
    unique_yr = {}
    cwd = os.getcwd()
    for fd in file_dates:
        if fd.year in unique_yr:
            if fd.month in unique_yr[fd.year]:
                continue
            else:
                unique_yr[fd.year].append(fd.month)
        else:
            unique_yr[fd.year] = []
            unique_yr[fd.year].append(fd.month)
    for year in unique_yr:
        files_present = os.listdir(cwd + '/' + str(year))
        for file in files_present:
            for field in fields:
                for fd in tqdm(file_dates):
                    cwd = os.getcwd()
                    f = netCDF4.Dataset(cwd+'/'+str(year)+"/"+file)
                    variable = f.variables[field]
                    print(f.variables.keys())
                    write_field(variable, field, fd)


def create_directory(date):
    cwd = os.getcwd()
    if not os.path.exists(cwd+'/'+str(date.year)):
        os.makedirs(cwd+'/'+str(date.year))


def organize_files(file_list):
    merra_file = []
    date_app = []
    cwd = os.getcwd()
    for file in file_list:
        if '.nc4' in file:
            merra_file.append(file)
            name = file.split('.')
            d = datetime.strptime(name[2], "%Y%m%d").date()
            date_app.append(d)
            create_directory(d)
            print(d)
    for mf, d in zip(merra_file, date_app):
        shutil.move(DOWNLOAD+'/'+mf, cwd+"/"+str(d.year))
    return date_app


def check_download_status():

    while True:
        finished = True
        file_list = os.listdir(DOWNLOAD)
        for file in file_list:
            if '.crdownload' in file:
                finished = False
        if finished:
            break

    date_app = organize_files(file_list)
    return date_app


def download_data(all_date):

    date_link = {2019: {},
                2018 : {},
                2017 : {},
                2016 : {}}

    for d in all_date:
        yr = d.year
        month = d.month
        day = d.day
        if yr in date_link:
            if month in date_link[yr]:
                print("if the month is present in the date link with that particular year")
                date_link[yr][month].append(day)
            else:
                date_link[yr][month] = []
                date_link[yr][month].append(day)

    return date_link


def get_all_dates(from_data, to_date):
    f_year, f_month, f_day = from_data.split('-')
    t_year, t_month, t_day = to_date.split('-')
    sdate = date(int(f_year), int(f_month), int(f_day))  # start date
    edate = date(int(t_year), int(t_month), int(t_day))  # end date
    delta = edate - sdate  # as timedelta
    dates = []

    for i in range(delta.days + 1):
        day = sdate + timedelta(days=i)
        dates.append(day)

    return dates


def get_data(driver, all_dates):
    td_yr_count = 40
    td_month_count = 4

    for yr in all_dates:
        if not all_dates[yr]:
            continue
        index = yr_corr[yr]
        td_yr = td_yr_count + index
        year_link = driver.find_elements_by_xpath("/html/body/table/tbody/tr["+str(td_yr)+"]/td[2]/a")
        year_link[0].click()
        for month in all_dates[yr]:
            td_month = td_month_count+(month-1)
            month_link = driver.find_elements_by_xpath("/ html / body / table / tbody / tr["+str(td_month)+"] / td[2] / a")
            month_link[0].click()
            for day in all_dates[yr][month]:
                td_day = (day+1)*2
                day_link = driver.find_elements_by_xpath("/html/body/table/tbody/tr["+str(td_day)+"]/td[2]/ a")
                day_link[0].click()

    file_dates = check_download_status()
    return file_dates


def login(alldates, co):
    check_download_status()
    browser = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver", chrome_options=co)
    """ Visits website and inserts login information"""
    browser.get(EARTH_DATA_LINK)
    user_name = browser.find_element_by_id('username')
    user_name.send_keys(USERNAME)
    password = browser.find_element_by_id("password")
    password.send_keys(PASSWORD)
    password.send_keys(Keys.ENTER)

    """Now searching the data set"""
    browser.implicitly_wait(500)
    buttons = browser.find_elements_by_xpath("// html/body/div[1]/div/div/div[1]/button")
    buttons[0].click()

    data_search = browser.find_element_by_id("intro-step-3")
    data_search.send_keys(DATA_FILE)
    data_search.send_keys(Keys.ENTER)

    data_click = browser.find_elements_by_xpath("// *[ @ id = 'intro-search-3-0'] / span[1]")
    data_click[0].click()
    online_archive = browser.find_elements_by_xpath("// *[ @ id = 'intro-dt-5']/div[1]/div/a")
    online_archive[0].click()
    data_link = online_archive[0].get_attribute("href")
    """ Switching tab"""
    allTabs = browser.window_handles
    browser.switch_to.window(browser.window_handles[1])
    file_dates = get_data(browser, alldates)

    return file_dates


def run():

    file_dates = check_download_status()#This is just a temporary function placement for quick debugging
    chromeOptions = webdriver.ChromeOptions()

    prefs = {"download.default_directory": "home/amman/PycharmProjects/merra-weather"}
    chromeOptions.add_experimental_option("prefs", prefs)
    all_dates = get_all_dates(FROM_DATE, TO_DATE)
    data_link = download_data(all_dates)
    file_dates = login(data_link, chromeOptions)
    writing_files_to_csv(file_dates)


if __name__ == '__main__':
    run()
